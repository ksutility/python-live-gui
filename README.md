# Python Live GUI

A simple python immediate mode gui that renders
out to the Web.

This GUI renders your scripts to an html page which you
can then view on remote machines.

The current primary usecase of the script
is for simple tasks that you want a gui for.
These pages can easily be port fowarded
through ssh to view on a different
machine from the server that is being
used to run the script.

Tested with python 3.7.

You can try it out by installing with pip
from the repository:

```sh
pip3 install git+https://gitlab.com/BenWiser/python-live-gui.git
```

## How does the code look?

It's as simple as:

```python
from live_gui import draw, state, button, text

@state
class State:
    count = 0

@draw("Example App")
def myApp(state: State):
    if button("-"):
        state.count -= 1

    text("The count is %d" % state.count)

    if button("+"):
        state.count += 1
```

This gets rendered to localhost:7070 by default.

This is how it looks in dark mode:

![example](example.gif)

This will automatically rerender the needed components when
the count is updated.

Look in [examples](./examples/) to see further API usage.

## How does it work?

This spawns up a new thread for each new
page open. It then uses http streaming
to keep the page in sync with your new
app.

The API will send JSON containing the
html elements that need to be updated.

This is purposefully opinionated about
how things should be styled.
This library is intended for writing
quick scripts as oppose to full applications.

It takes some insparation from
dear imGUI in how the API looks
for rendering.

## Development instructions

Run tests with the command `python3 -m unittest`.

## Design Decisions

### No third party dependencies

This intentionally uses no third party libraries
so that I can easily reference a git url and use
this library without having to do much set up.

### HTTP Streaming for updates

I did consider using web sockets but I landed on
HTTP streaming because it's a bit easier to
implement and follow with standard libraries
and because I expect the server to normally
be doing more processing and updates than I expect
GUI interactions.

I'm also not too concerned about ultra optimising
performance because this isn't meant to serve
millions of clients.

### Separate threads for each client

I'm doing this simply to make clear memory separations
between each client currently. All client
responses actually use the same thread lock.
This could be improved by making each client have
it's own thread lock.

This isn't meant to serve thousands of users however,
this is meant to be a gui for your own remote servers
so this isn't a major deal.

It does mean that if you have multiple windows
open for the same app, you'll have to be
careful about long running operations in your
render method.

### Render IDs

The render graph knows what to rerender using the
render ids.

It builds this from the component name and render order.

### Global state component APIs

I decided to switch away from using the app API methods
directly for rendering components so that it would be
easy to create functions that could separately
render without needing to know about the app.

This is really nice as well because it means the
state per app is really easy to define as a single
global object. This means users don't have to worry
about API methods like on load. They can just have
a single strong contract for their app state.
