from components.button import Button
from components.column import Column
from components.label_input import LabelInput
from components.list_item import ListItem
from components.list import List
from components.menu_bar import MenuBar
from components.row import Row
from components.text import Text

class ComponentManager:
    def __init__(self):
        self.button = Button()
        self.column = Column()
        self.label_input = LabelInput()
        self.list = List()
        self.list_item = ListItem()
        self.menu_bar = MenuBar()
        self.row = Row()
        self.text = Text()

        self.components = [
            self.button,
            self.column,
            self.label_input,
            self.list_item,
            self.list,
            self.menu_bar,
            self.row,
            self.text,
        ]

    def reset(self):
        for component in self.components:
            component.reset()

    def iter(self):
        for component in self.components:
            yield component