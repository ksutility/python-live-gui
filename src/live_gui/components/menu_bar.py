from components.component import Component, GraphComponent

class MenuBar(Component):
    def get_name(self) -> str:
        return 'menu_bar'

    def render(self, id: int, button) -> str:
        yield '<div id="{0}" class="menu_bar">'.format(id)
        yield '</div>'
