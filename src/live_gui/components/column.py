from components.component import Component, GraphComponent

class Column(Component):
    def get_name(self) -> str:
        return 'column'

    def render(self, id: int, button) -> str:
        yield '<div id="{0}" class="column">'.format(id)
        yield '</div>'
