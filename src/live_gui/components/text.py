from components.component import Component, GraphComponent

class Text(Component):
    def get_name(self) -> str:
        return 'text'

    def add_to_render_graph(self, id: int, text: str):
        return None, GraphComponent(
            render_id = id,
            name = self.get_name(),
            args = [text])

    def render(self, id: int, text) -> str:
        return (
            '<p id="{0}">'
            '{1}'
            '</p>'
        ).format(id, text.args[0])