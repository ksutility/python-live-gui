from collections import namedtuple

class Component:
    """ Implement this class when you want to make a new
    GUI component and then add your components to the GUI elements in
    __init__.py
    """

    def get_name(self) -> str:
        raise Exception('Must implement get_name')

    def add_to_render_graph(self, id: int):
        return None, GraphComponent(
            render_id = id,
            name = self.get_name(),
            args = [])

    def reset(self) -> None:
        pass

    def do_action(self, id: int, value) -> None:
        pass
    
    def get_action(self, id: int, default):
        pass

    def render(self, component) -> str:
        raise Exception('Must implement render')


GraphComponent = namedtuple('GraphComponent', "name render_id args")