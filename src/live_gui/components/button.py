from components.component import Component, GraphComponent

class Button(Component):

    __slots__ = ("_pressed_buttons")

    def __init__(self):
        self._pressed_buttons = {}

    def get_name(self) -> str:
        return 'button'

    def add_to_render_graph(self, id: int, text: str):
        action = self.get_action(id)

        return action, GraphComponent(
            render_id = id,
            name = self.get_name(),
            args = [text])

    def reset(self) -> None:
        self._pressed_buttons = {}

    def do_action(self, id: int) -> None:
        self._pressed_buttons[id] = True

    def get_action(self, id: int):
        return self._pressed_buttons.get(id, False)

    def render(self, id: int, button) -> str:
        return (
            '<button id="{0}" onClick="doAction(\'{1}\', \'{2}\')">{3}</button>'
        ).format(id, self.get_name(), button.render_id, button.args[0])