class App:
    """ This app is what every new script should inhert and override
    it provides simple gui methods
    """

    def __init__(self, render_graph):
        self.render_graph = render_graph

    def render(self):
        raise Exception('Render method not overriden')

    def _button(self, text: str) -> bool:
        return self.render_graph.add_button(text)

    def _label_input(self, label: str, default: str) -> str:
        return self.render_graph.add_label_input(label, default)

    def _text(self, text: str) -> None:
        self.render_graph.add_text(text)

    def _start_column(self) -> None:
        self.render_graph.open_column_children()
    
    def _end_column(self) -> None:
        self.render_graph.close_column_children()

    def _start_row(self) -> None:
        self.render_graph.open_row_children()
    
    def _end_row(self) -> None:
        self.render_graph.close_row_children()

    def _start_list(self) -> None:
        self.render_graph.open_list_children()
    
    def _end_list(self) -> None:
        self.render_graph.close_list_children()

    def _start_list_item(self) -> None:
        self.render_graph.open_list_item_children()
    
    def _end_list_item(self) -> None:
        self.render_graph.close_list_item_children()

    def _start_menu_bar(self) -> None:
        self.render_graph.open_menu_bar_children()
    
    def _end_menu_bar(self) -> None:
        self.render_graph.close_menu_bar_children()

    def _private_render(self) -> str:
        self.render_graph.init()
        self.render()
        return self.render_graph.perform_render()
