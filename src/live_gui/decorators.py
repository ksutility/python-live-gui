import inspect
import atexit

from copy import deepcopy

from base_app import App
from http_listener import startApp

STATE_ARGS = None

def state(cls):
    global STATE_ARGS

    if STATE_ARGS is not None:
        raise Exception("@state can only be called once")

    STATE_ARGS = inspect.getmembers(cls)
    # Remove args with private and dunders
    STATE_ARGS = [arg for arg in STATE_ARGS if not(arg[0].startswith('__') and arg[0].endswith('__'))]

def createState():
    global STATE_ARGS

    class State():
        pass

    state = State()

    if STATE_ARGS is not None:
        for key, value in STATE_ARGS:
            setattr(state, key, deepcopy(value))

    return state

def draw(app_name, port: int = 7070):
    def decorator(draw_func):
        class NewApp(App):
            def __init__(self, render_graph):
                self.state = createState()

                super().__init__(render_graph)

            def render(self):
                draw_func(self.state)

        # Register to start the API on exit so that the rest of the script
        # can finish executing
        atexit.register(lambda: startApp(app_name, NewApp, port=port))

    return decorator
