from threading import get_ident

from base_app import App

class AppStateManager:
    """This class manages the mapping between apps and response id
    this allows us to have the global component API because it knows which app to
    call this against"""
    def __init__(self):
        self._apps = {}
        self._ids_for_threads = {}

    def add_app(self, response_id: int, app: App):
        self._apps[response_id] = app

    def set_response_id_for_current_thread(self, response_id: int):
        self._ids_for_threads[get_ident()] = response_id

    def unset_response_id_for_current_thread(self):
        del self._ids_for_threads[get_ident()]

    def get_current_app(self) -> App:
        response_id = self._ids_for_threads[get_ident()]
        return self._apps[response_id]

APP_MANAGER = AppStateManager()
