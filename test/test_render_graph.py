from unittest import TestCase

from src.live_gui.render_graph import RenderGraph
from src.live_gui.components import ComponentManager

class TestRenderGraph(TestCase):
    def test_has_render_APIs_for_all_components(self):
        component_manager = ComponentManager()
        render_graph = RenderGraph()

        for component in component_manager.iter():
            self.assertTrue(hasattr(render_graph, 'add_%s' % component.get_name()))
            self.assertTrue(hasattr(render_graph, 'open_%s_children' % component.get_name()))
            self.assertTrue(hasattr(render_graph, 'close_%s_children' % component.get_name()))

    

if __name__ == '__main__':
    unittest.main()