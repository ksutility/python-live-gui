from unittest import TestCase

from src.live_gui.server_state import ServerState

class TestApp:
    def __init__(self, graph):
        pass

class TestServerState(TestCase):
    def test_add_response_increments_response_ids(self):
        server_state = ServerState(TestApp)
        for i in range(10):
            self.assertEqual(server_state.add_response(), i)
        
    def test_get_app(self):
        server_state = ServerState(TestApp)
        response_id = server_state.add_response()
        app = server_state.get_app(response_id)

        self.assertIsInstance(app, TestApp)
    
    def test_delete_id(self):
        server_state = ServerState(TestApp)
        response_id = server_state.add_response()
        server_state.delete_id(response_id)

        self.assertRaises(KeyError, lambda: server_state.get_app(response_id))

if __name__ == '__main__':
    unittest.main()