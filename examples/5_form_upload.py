#!python3

import sys
import pathlib

sys.path.append((pathlib.Path(__file__).parent.parent / 'src').as_posix())

from live_gui import draw, state
from live_gui import (
    button,
    text,
    label_input,
    column
)

@state
class State:
    is_saved = False
    name = ""

@draw("Example app")
def myApp(state: State):
    if not state.is_saved:
        with column():
            text("Demo of saving data to a file")

            state.name = label_input("What is your name?", state.name)

            if button("Save"):
                with open("./names.txt", mode="+a") as file:
                    file.write(f"{state.name}\n")

                state.is_saved = True
    else:
        text("Thanks for submitting your name!")
