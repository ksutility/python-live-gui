#!python3

import sys
import pathlib
sys.path.append((pathlib.Path(__file__).parent.parent / 'src').as_posix())

from live_gui import state, draw
from live_gui import (
    button,
    column,
    label_input,
    row,
    text,
)

@state
class State:
    toggle = False
    first_name = "Billy"
    second_name = "Bob"

@draw("Example App")
def myApp(state: State):
    with column():
        state.first_name = label_input('First name', state.first_name)

        text('Your name is %s ' % state.first_name + state.second_name)

        with row():
            text("Do you want to toggle the button?")

            if button("Button on" if state.toggle else "Button off"):
                state.toggle = not state.toggle

    with column():
        state.second_name = label_input('Second name', state.second_name)
