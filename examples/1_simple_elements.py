#!python3

import sys
import pathlib
sys.path.append((pathlib.Path(__file__).parent.parent / 'src').as_posix())

from live_gui import state, draw
from live_gui import (
    button,
    column,
    label_input,
    text,
)

# This is the state of your app
# It will be initialised for every new window opened
# and provided to render
@state
class State:
    toggle = False
    name = 'Billy'

# Simply pass your app name and call with with
# the function to render
@draw("Example App")
def myApp(state: State):
    with column():
        state.name = label_input('What is your name', state.name)

        # This is the most simple API
        # it will just render whatever text you give it
        text('Your name is %s ' % state.name)

        # The button element will return true if it was pressed
        if button("Button on" if state.toggle else "Button off"):
            state.toggle = not state.toggle
